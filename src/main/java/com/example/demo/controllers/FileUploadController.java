package com.example.demo.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.example.demo.data.FileSplit;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
public class FileUploadController {


    @GetMapping("/")
    public String index() {
        return "uploadForm";
    }


    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws IOException {

        String current = new java.io.File( "." ).getCanonicalPath();
        System.out.println("Current dir:"+current);
        String uploadedFolder= current + "/uploadedFiles/";

        //clean up directory
        FileUtils.cleanDirectory(new File(uploadedFolder));

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/";
        }

        try {


            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(uploadedFolder + file.getOriginalFilename());
            Files.write(path, bytes);

            //Split the file
            FileSplit.splitFile(path.toFile());

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            redirectAttributes.addFlashAttribute("message",
                    "Please make sure that this folder exists in your local '" + uploadedFolder + "'");
            e.printStackTrace();
        }

        return "redirect:/";
    }



}