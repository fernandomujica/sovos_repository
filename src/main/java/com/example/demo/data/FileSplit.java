package com.example.demo.data;


import java.io.*;

public class FileSplit {



    public static void splitFile(File f) throws IOException {
        int partCounter = 1;

        //Get the file and split randomize between 1 and 20
        int splitNumber = (int) (Math.random() * 20) + 1;
        System.out.println("Random number:"+splitNumber);

        int sizeOfFiles = (int) f.length();
        final int bytesPerSplit = sizeOfFiles/splitNumber;
        final long remainingBytes = sizeOfFiles % bytesPerSplit;


        byte[] buffer = new byte[bytesPerSplit];

        String fileName = f.getName();

        //try-with-resources to ensure closing stream
        try (FileInputStream fis = new FileInputStream(f);
             BufferedInputStream bis = new BufferedInputStream(fis)) {

            int bytesAmount = 0;
            while ((bytesAmount = bis.read(buffer)) > 0) {
                //write each chunk of data into separate file with different number in name .001 .002 etc extension
                String filePartName = String.format("%s.%03d", fileName, partCounter++);
                File newFile = new File(f.getParent(), filePartName);
                try (FileOutputStream out = new FileOutputStream(newFile)) {
                    out.write(buffer, 0, bytesAmount);
                }
            }
        }
    }

}