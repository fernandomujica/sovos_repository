Prerequisitos
-------------
-Tener Git Client instalado , sino bajarlo desde aqui
- "https://git-scm.com/downloads"


- Abrir la consola de git (Git Bash) y en la raiz disco "C:" ejecutar el siguiente comando
- "git clone https://fernandomujica@bitbucket.org/fernandomujica/sovos_repository.git"
- Esperar que se baje todo el proyecto desde Bitbucket Repository

Ejecucion
---------
- Una vez que el proyecto se haya bajado al "C:"
- Abrir Command Prompt (CMD) desde windows
- Ir a "C:\sovos_repository"
- Ejecutar "java -jar target/demo-0.0.1-SNAPSHOT.jar" en el directorio "C:\sovos_repository\"
- SpringBoot se ejecutara y arrancara la aplicacion automaticamente (para parar el servidor apretar ctrl+c)
- Abrir el browser y escribir "http://localhost:8080/"   (** Si por algun motivo la aplicacion no esta desplegada asegurarse que ningun servicio este ocupando el puerto 8080, una vez hecho lo anterior ir al command prompt de windows donde esta corriendo springboot ejecutar ctrl+c y luego "java -jar target/demo-0.0.1-SNAPSHOT.jar" y reintentar)
- Elegir cualquier archivo desde el pc y hacer click en "Upload"
- Abrir Otro Command Prompt (CMD) desde windows (ya que el anterior se esta ejecuando el servidor)
- Ir a "C:\sovos_repository\uploadedFiles"
- Ahi se encontraran los archivos separados en partes iguales, por tanto ejecutar en windows 
        "C:\>copy /b" como corresponda para unirlos
        - *En entorno linux ejecutar comando "cat" en vez de "copy /b"
        
Compilar y ejecutar desde los fuentes
--------------------------------------
- Tener Maven instalado , si no lo tienes puede bajar la ultima version desde aca 
- http://www-us.apache.org/dist/maven/maven-3/3.5.3/binaries/apache-maven-3.5.3-bin.zip
- Descomprimir "apache-maven-3.5.3-bin.zip" en "c:" y agregarlo al PATH de windows

- Abrir Command Prompt (CMD)    
- ir a "C:\sovos_repository\"
- ejecutar "mvn clean package" en el directorio "C:\sovos_repository\"
- ejecutar "java -jar target/demo-0.0.1-SNAPSHOT.jar" en el directorio "C:\sovos_repository\"
- SpringBoot se ejecutara y arrancara la aplicacion automaticamente (para parar el servidor apretar ctrl+c)
- Abrir el browser y escribir "http://localhost:8080/"   (** Si por algun motivo la aplicacion no esta desplegada asegurarse que ningun servicio este ocupando el puerto 8080, una vez hecho lo anterior ir al command prompt de windows donde esta corriendo springboot ejecutar ctrl+c y luego "java -jar target/demo-0.0.1-SNAPSHOT.jar" y reintentar)
- Elegir cualquier archivo desde el pc y hacer click en "Upload"
- Abrir Otro Command Prompt (CMD) desde windows (ya que el anterior se esta ejecuando el servidor)
- ir a "C:\sovos_repository\uploadedFiles"
- Ahi se encontraran los archivos separados en partes iguales, por tanto ejecutar en windows 
        "C:\>copy /b" como corresponda para unirlos
        - *En entorno linux ejecutar comando "cat" en vez de "copy /b"
	


